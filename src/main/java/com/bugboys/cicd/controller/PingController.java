package com.bugboys.cicd.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * @Version 1.0
 * --------------------------------------------------------- *
 * @Description >_
 * --------------------------------------------------------- *
 * @Author Ape
 * --------------------------------------------------------- *
 * @Email <16511660@qq.com>
 * --------------------------------------------------------- *
 * @Date 2019-07-30  09:18
 * --------------------------------------------------------- *
 */
@RestController
public class PingController {

    @RequestMapping("ping")
    @ResponseBody
    public Object run(){
        HashMap<String, Object> map = new HashMap<>();
        map.put("ping", "ok");
        map.put("time", "10.08");
        return map;
    }

}

package com.bugboys.cicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BugboysSpringbootCiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BugboysSpringbootCiApplication.class, args);
	}

}

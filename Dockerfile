FROM openjdk:8-jre-alpine

LABEL maintainer="ape" \
    description="基于java1.8 springboot-ci"

WORKDIR /root/

RUN mkdir -p /root && \
    echo "Asia/Shanghai" > /etc/timezone

COPY target/bugboys-*.jar /root/app.jar
EXPOSE 8080

CMD java -Djava.security.egd=file:/dev/./urandom -jar app.jar --spring.profiles.active=prod
